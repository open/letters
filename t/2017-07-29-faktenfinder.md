date: 2017-07-29
sender: "base64.decodebytes(b'Y2Fyc3Rlbi5rbm9sbEBwb3N0ZW8uZGU=')"
recipient: https://www.tagesschau.de/mehr/kontakt/kontakt-web-101.html
tags: media, democracy, sources

---

Sehr geehrte Damen und Herren,

Ich halte die Existenz des Formats "Faktenfinder" für sehr gut. Die Umsetzung ist bisher meiner Meinung nach aber nur teilweise gelungen. Mit den folgenden Anmerkungen möchte ich zu einem Verbesserungsprozess beitragen.

1. Fragwürdig ist für mich, warum gerade bei diesem Format im Gegensatz zu "normalen" Beiträgen auf tagesschau.de keine Kommentarfunktion existiert. Das Format heißt ja schließlich *"Faktenfinder"* und nicht *"Faktenverkünder"*. Da die TS-Redaktion (zum Glück) keinen Anspruch auf Allwissenheit und Unfehlbarkeit erhebt, kann es ohnehin nur eine *Diskurswahrheit* geben. Um diesen Diskurs-Prozess zu ermöglichen, braucht es aber einen Rück-Kanal - auch wenn die Moderation Geld kostet. Dafür bezahlen wir schließlich auch Rundfunk-Beiträge.
2. Der Beitrag [US-Angriff auf den Irak 2003 - Wie berichtete die ARD damals?][1] sollte die zitierten Blogs auch klar benennen. Nachvollziehbare Quellen sind nun einmal das Rückrat jeder soliden Argumentation. Das Weglassen von Quellenangaben sollte eigentlich ein No-Go für ein Medium sein, was sich offensiv gegen das Phänomen der Fake-News positioniert. Ganz ähnlich argumentiert ja Tina Hassel in ihrem Kommentar zu Powells Auftritt im Sicherheitsrat.
3. Tatsächlich könnte ein *Leitmedium wie die Tagesschau* generell (also nicht nur im Faktenfinder-Format) *mit umfassenden und nachvollziehbaren Quellenangaben mediale Maßstäbe setzen*. Die derart etablierte Erwartungshaltung wäre gewissermaßen ein Bollwerk gegen Falschbehauptungen und Verzerrungen aus der Fake-News-Szene.
4. Dass die offene und deutliche Zustimmung der heutigen Bundeskanzlerin (damals in der Opposition) zum US-Irak-Einsatz mit keiner Silbe erwähnt wird, ist kein Pluspunkt für den Beitrag, sondern tendenziell eher eine Bestärkung der verbreiteten These, die Mainstream-Medien würden prinzipiell regierungsnah berichten. Es hätte den Beitrag weniger eindeutig gemacht. Aber gerade das Faktenfinder-Format habe ich bisher als Rahmen wahrgenommen, in dem die Ambivalenzen einer komplexen Welt offen angesprochen werden können.

Ich hoffe und vermute, dass es zumindest zu diesem Punkt Diskussionen innerhalb der Redaktion gab und möchte – verbunden mit einem Appell an Ihr Verantwortungsbewusstsein – davor  warnen, unbequeme Aspekte der Realität einfach auszublenden.

Über eine (zumindest kurze) Rückmeldung würde ich mich freuen.

Mit freundlich Grüßen,
...

[1]: http://faktenfinder.tagesschau.de/ausland/irak-krieg-berichterstattung-101.html




