# Open Letters

## General Information

There are many lamentable issues in our society. A considerable fraction of these could be solved if the *right people* would be aware of the *right information*. Filing an application or writing a letter of complaint is thus a traditional and rational reaction to perceived problems. However, very often such writings remain unnoticed and die away unheeded. To increase the priority of these messages, publishing them is a able strategy: If the message reaches enough people the pressure of public opinion strongly incentivizes the original addressee to react. It also contributes to the exchange of ideas and arguments in civil society, and helps it organizing. Furthermore, knowing that a text will be publicly available and thus might contribute to the authors public identity usually increases their motivation and hence the quality of the writing.

This repository is an experimental place for such writings. It might help to overcome the nontrivial problem of finding a reliable public habitat for open letters. It might also be a place where some basic review can take place.

## Rules

The following rules should achieve that this medium contributes to the solution of societal problems and does not become problematic itself.

### General

- A published text should serve some aspect of the common interest of the whole humanity.
- A published text should address a specific problem and propose a concrete measure to counter that problem.
- A published text must not insult or demean individuals or groups of humans.
- A published text should be written in a constructive and respectful tone.

### Technical

- A published text is written in markdown format with a YAML-header.
- The YAML-header contains meta-data: sending-date, sender, recipient, tags, language
- Publication takes place via pull-requests against the main branch of the repo. A merged pull request is a contribution, the author is a contributor.
- To be merged into main each pull request needs at least 20% positive votes of earlier contributors. Each negative vote needs to be compensated by two additional positive votes, which are not counted for the 20% quorum.
- Each contributor agrees to vote at least three times on other contributions for each merged pull request.
